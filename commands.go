package main

import (
	"errors"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"gopkg.in/alecthomas/kingpin.v2"
	"net/url"
	"os"
	"sync"
	"time"
)

func InitAction(context *kingpin.ParseContext) error {
	err := InitializeDatabase()
	if err != nil {
		return err
	}

	return nil
}

type Command interface {
	Run(ctx *kingpin.ParseContext) error
}

type AddCommand struct {
	Domain string
	Receiver string
	Channel string
}

func (c *AddCommand) Run(context *kingpin.ParseContext) error {
	db, err := ReadDatabase()
	if err != nil {
		return err
	}

	host, err := url.Parse(c.Domain)
	if err != nil {
		return err
	}

	if host.Scheme != "https" {
		return errors.New("domain must be https://")
	}

	if CheckIfReceiverUrlComboExists(db, c.Receiver, c.Domain) {
		return errors.New("domain/receiver combo already exists")
	}

	uuid, err := GenerateId()
	if err != nil {
		return err
	}

	domain := &Domain{
		Id:             uuid,
		Url:            c.Domain,
		NotifyChannel:  c.Channel,
		NotifyReceiver: c.Receiver,
		Failures: 0,
	}

	db[uuid] = *domain

	err = SaveDatabase(db)
	return err
}

type RemoveByIdCommand struct {
	Id string
}

func (r *RemoveByIdCommand) Run(ctx *kingpin.ParseContext) error {
	db, err := ReadDatabase()
	if err != nil {
		return err
	}

	if _, ok := db[r.Id]; ok {
		delete(db, r.Id)
	} else {
		return errors.New("invalid id")
	}

	err = SaveDatabase(db)
	if err != nil {
		return err
	}

	return nil
}

type LsCommand struct {}

func (l *LsCommand) Run(ctx *kingpin.ParseContext) error {
	db, err := ReadDatabase()
	if err != nil {
		return err
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Domain", "Channel", "Receiver", "Expires", "Failures"})

	table.SetBorder(false)
	table.SetCenterSeparator("")
	table.SetColumnSeparator("")
	table.SetRowSeparator("")
	table.SetTablePadding("\t")
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.SetHeaderLine(false)

	for _, val := range db {
		table.Append([]string{val.Id, val.Url, val.NotifyChannel, val.NotifyReceiver, val.Expires.Format(time.RFC3339), fmt.Sprintf("%d", val.Failures)})
	}
	table.Render()
	return nil
}

type CheckCommand struct {
	Verbose bool
}

func (c *CheckCommand) Run(ctx *kingpin.ParseContext) error {
	var MAX_CONCURRENCY = 10

	db, err := ReadDatabase()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	var tasks = make(chan Domain, len(db))

	var toRemove []Domain

	for i := 0; i < MAX_CONCURRENCY; i++ {
		wg.Add(1)
		go func() {
			for domain := range tasks {
				if Verbose {
					fmt.Println("testing domain", domain.Url)
				}
				checkedDomain, err := FetchUrl(domain, true)
				if err != nil {
					fmt.Printf("failed to test url: %v\n", err)
				}

				if Verbose {
					fmt.Println("domain failures:", checkedDomain.Failures)
				}

				if checkedDomain.Failures >= 7 {
					toRemove = append(toRemove, checkedDomain)
				}

				db[checkedDomain.Id] = checkedDomain
			}
			wg.Done()
		}()
	}

	for _, domain := range db {
		tasks <- domain
	}
	close(tasks)
	wg.Wait()

	for _, domain := range toRemove {
		fmt.Println("removing domain", domain.Url)
		delete(db, domain.Id)
	}

	err = SaveDatabase(db)
	if err != nil {
		return err
	}

	return nil
}

type CheckSingleCommand struct {
	Url string
}

func (c *CheckSingleCommand) Run(ctx *kingpin.ParseContext) error {
	host, err := url.Parse(c.Url)
	if err != nil {
		return err
	}

	if host.Scheme != "https" {
		return errors.New("domain must be https://")
	}

	domain := Domain{
		Id:             "",
		Url:            c.Url,
		NotifyChannel:  "",
		NotifyReceiver: "",
		Failures:       0,
		Expires:        time.Time{},
	}

	domain, err = FetchUrl(domain, false)
	if err != nil {
		return err
	}

	if domain.Failures == 0 {
		fmt.Printf("Domain %s checked, expires: %s\n", domain.Url, domain.Expires)
	}

	return nil
}


