package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

var SlackUrl string
var Verbose bool

func main() {
	app := kingpin.New("certmon", "A Certificate monitoring application")

	app.Command("init", "Initialize the database").Action(InitAction)


	addCmd := &AddCommand{}
	add := app.Command("add", "Add domain to monitor").Action(addCmd.Run)
	add.Arg("domain", "The domain to add").Required().StringVar(&addCmd.Domain)
	add.Arg("notify-channel", "The channel to notify through").Required().StringVar(&addCmd.Channel)
	add.Arg("notify-receiver", "The receiver of the notification").Required().StringVar(&addCmd.Receiver)

	rmIdCmd := &RemoveByIdCommand{}
	rmId := app.Command("rm", "Remove domain by id").Action(rmIdCmd.Run)
	rmId.Arg("id", "The domain ID (uuid)").Required().StringVar(&rmIdCmd.Id)

	lsCmd := &LsCommand{}
	app.Command("ls", "List domains").Action(lsCmd.Run)

	checkCmd := &CheckCommand{}
	check := app.Command("check", "Check Certificates").Action(checkCmd.Run)
	check.Flag("verbose", "Verbose output").BoolVar(&Verbose)
	check.Flag("slack-url", "Legacy incoming slack webhook").StringVar(&SlackUrl)

	checkSingleCmd := &CheckSingleCommand{}
	checkSingle := app.Command("check-single", "Check a single domain certificate").Action(checkSingleCmd.Run)
	checkSingle.Arg("url", "The URL to check").StringVar(&checkSingleCmd.Url)

	kingpin.MustParse(app.Parse(os.Args[1:]))
}
