package main

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

const DatabaseName = "domains.json"

type Domain struct {
	Id string `json:"id"`
	Url string `json:"url"`
	NotifyChannel string `json:"notify_channel"`
	NotifyReceiver string `json:"notify_receiver"`
	Failures int `json:"failures"`
	Expires time.Time `json:"expires"`
}

type Domains map[string]Domain

func GenerateId() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	uuid := fmt.Sprintf(
		"%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:],
	)

	return uuid, nil
}

func InitializeDatabase() error {
	result := Domains{}

	_, err := os.Stat(DatabaseName)
	if err == nil {
		return errors.New("database file already exists")
	}

	f, err := os.Create(DatabaseName)
	if err != nil {
		return err
	}

	err = json.NewEncoder(f).Encode(result)
	if err != nil {
		return err
	}

	return nil
}

func ReadDatabase() (Domains, error) {
	result := Domains{}
	fileData, err := ioutil.ReadFile(DatabaseName)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(fileData, &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func SaveDatabase(db Domains) error {
	err := os.Truncate(DatabaseName, 0)
	if err != nil {
		return err
	}

	file, err := os.OpenFile(DatabaseName, os.O_WRONLY, 0755)
	if err != nil {
		return err
	}

	err = json.NewEncoder(file).Encode(db)
	if err != nil {
		return err
	}

	return nil
}
