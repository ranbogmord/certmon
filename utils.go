package main

import (
	"bytes"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"reflect"
	"time"
)

func CheckIfReceiverUrlComboExists(db map[string]Domain, receiver, url string) bool {
	for _, domain := range db {
		if domain.Url == url && domain.NotifyReceiver == receiver {
			return true
		}
	}

	return false
}

func FetchUrl(domain Domain, sendToSlack bool) (Domain, error) {
	uri, err := url.Parse(domain.Url)
	if err != nil {
		domain.Failures += 1
		return domain, errors.New("failed to parse url")
	}

	httpsUrl := fmt.Sprintf("https://%s/", uri.Host)
	res, err := http.Get(httpsUrl)
	if err != nil {
		domain.Failures += 1

		if reflect.TypeOf(err).String() == "*url.Error" {
			reqErr := err.(*url.Error)

			errType := reflect.TypeOf(reqErr.Err).String()

			if errType == "x509.CertificateInvalidError" {
				parsedErr := reqErr.Err.(x509.CertificateInvalidError)

				// TODO: Send message using slack/email
				if sendToSlack {
					err = SendSlackNotification(domain, "Expired", parsedErr.Error())
				} else {
					fmt.Printf("[%s] Expired: %s\n", domain.Url, parsedErr.Error())
					err = nil
				}
				return domain, err
			} else if errType == "x509.HostnameError" {
				parsedErr := reqErr.Err.(x509.HostnameError)

				if sendToSlack {
					err = SendSlackNotification(domain, "Invalid hostname", parsedErr.Error())
				} else {
					fmt.Printf("[%s] Invalid hostname: %s\n", domain.Url, parsedErr.Error())
					err = nil
				}
				return domain, err
			} else if errType == "x509.UnknownAuthorityError" {
				parsedErr := reqErr.Err.(x509.UnknownAuthorityError)

				if sendToSlack {
					err = SendSlackNotification(domain, "Unknown CA", parsedErr.Error())
				} else {
					fmt.Printf("[%s] Unknown CA: %s\n", domain.Url, parsedErr.Error())
					err = nil
				}

				return domain, err
			} else {
				return domain, reqErr.Err
			}
		} else {
			return domain, err
		}
	} else {
		// Check expiry date
		if len(res.TLS.PeerCertificates) > 0 {
			cert := res.TLS.PeerCertificates[0]

			domain.Expires = cert.NotAfter

			diff := cert.NotAfter.Sub(time.Now())
			hours := diff.Hours()

			var dateDiff string
			if hours < 24 {
				// one day
				dateDiff = "today"
			} else if isBetween(24, hours, 48) {
				// two days
				dateDiff = "tomorrow"
			} else if isBetween(96, hours, 120) {
				// 5 days
				dateDiff = "in 5 days"
			} else if isBetween(144, hours, 168) {
				// 7 days
				dateDiff = "in 7 days"
			} else if isBetween(312, hours, 336) {
				// 14 days
				dateDiff = "in 14 days"
			} else if isBetween(696, hours, 720) {
				// 30 days
				dateDiff = "in 30 days"
			}

			if dateDiff != "" {
				msg := fmt.Sprintf("Certificate expires %s", dateDiff)

				if sendToSlack {
					err = SendSlackNotification(domain, "Expiry Warning", msg)
				} else {
					fmt.Printf("[%s] Expiry Warning: %s\n", domain.Url, msg)
					err = nil
				}

				return domain, err
			}
		}
	}

	return domain, nil
}

func isBetween(from float64, value float64, to float64) bool {
	return from < value && value < to
}

type SlackRequest struct {
	Channel string `json:"channel"`
	Text string `json:"text"`
}

func SendSlackNotification(domain Domain, subject string, errorString string) error {
	if SlackUrl == "" {
		return errors.New("no slack url defined")
	}

	msgTpl := "*[%s]: %s*\n\n%s"
	msg := fmt.Sprintf(msgTpl, domain.Url, subject, errorString)

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(SlackRequest{
		Channel: domain.NotifyReceiver,
		Text:    msg,
	})

	if err != nil {
		return err
	}

	res, err := http.Post(SlackUrl, "application/json", &buf)
	if err != nil {
		return err
	}

	if res.StatusCode != 200 {
		return errors.New("failed to send slack notification")
	}

	return nil
}
